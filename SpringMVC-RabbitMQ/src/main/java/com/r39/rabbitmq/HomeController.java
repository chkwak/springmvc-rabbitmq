package com.r39.rabbitmq;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.r39.rabbitmq.util.Message;
import com.r39.rabbitmq.util.RabbitmqClient;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.ShutdownSignalException;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

	public final String QUEUE_NAME = "TestQueue";

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	/**
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Model model) {
		logger.info("Welcome RabbitMQ Test home! ");

		HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
				.getRequest();
		String ipAddress = req.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null)
			ipAddress = req.getRemoteAddr();

		model.addAttribute("IP_Adress", ipAddress);

		
		return "home";
	}

	/**
	 * @param model
	 * @return
	 * @throws IOException
	 * @throws TimeoutException
	 */
	@RequestMapping(value = "/send", method = RequestMethod.GET)
	public String send(Model model) throws IOException, TimeoutException {
		logger.info("RabbitMQ Data send  ");

		RabbitmqClient client = new RabbitmqClient();
		Channel channel = client.getChannel();

		channel.queueDeclare(QUEUE_NAME, false, false, false, null);

		// create messages.
		List<Message> messages = new ArrayList<Message>();
		for (int i = 0; i < 10; i++) {
			messages.add(new Message(QUEUE_NAME, "Hello World! " + i));
		}

		System.out.println("ready to send.");
		for (Message m : messages) {
			channel.basicPublish(m.exchange, m.routingKey, null, m.body.getBytes());
			System.out.println(" [x] Sent " + m.toString());
		}

		client.close();
		model.addAttribute("Status", "Successful!!");
		
		return "home";
	}


}
