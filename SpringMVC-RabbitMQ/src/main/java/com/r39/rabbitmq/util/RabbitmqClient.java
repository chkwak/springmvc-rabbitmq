package com.r39.rabbitmq.util;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class RabbitmqClient {
	public static String HOST = "192.168.192.129";// RabbitMQ Server.
	public static String USNER_NAME = "chime";
	public static String USNER_PASSWD = "chime";
	public static String VIRTUAL_HOST = "TestService";
	public static int PORT = 5672;

	private Connection connection = null;
	private Channel channel = null;

	public Channel getChannel() throws IOException, TimeoutException {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(RabbitmqClient.HOST);
		factory.setUsername(USNER_NAME);
		factory.setPassword(USNER_PASSWD);
		factory.setVirtualHost(VIRTUAL_HOST);
		factory.setPort(PORT);
		this.connection = factory.newConnection();
		this.channel = connection.createChannel();

		return this.channel;
	}

	public void close() throws IOException, TimeoutException {
		this.channel.close();
		this.connection.close();
	}
}